<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\TypeUserRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=TypeUserRepository::class)
 */
class TypeUser
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $libelleTypeUser;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLibelleTypeUser(): ?string
    {
        return $this->libelleTypeUser;
    }

    public function setLibelleTypeUser(?string $libelleTypeUser): self
    {
        $this->libelleTypeUser = $libelleTypeUser;

        return $this;
    }
}
