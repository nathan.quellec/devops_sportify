<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210324153334 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        //$this->addSql('ALTER TABLE course DROP FOREIGN KEY FK_169E6FB9D12A823');
        //$this->addSql('ALTER TABLE repere_trajet DROP FOREIGN KEY FK_A59BDD6FD12A823');
        //$this->addSql('DROP TABLE repere_trajet');
        //$this->addSql('DROP TABLE trajet');
        //$this->addSql('DROP INDEX IDX_169E6FB9D12A823 ON course');
        //$this->addSql('ALTER TABLE course DROP trajet_id');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        //$this->addSql('CREATE TABLE repere_trajet (repere_id INT NOT NULL, trajet_id INT NOT NULL, INDEX IDX_A59BDD6F2ECB75C (repere_id), INDEX IDX_A59BDD6FD12A823 (trajet_id), PRIMARY KEY(repere_id, trajet_id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        //$this->addSql('CREATE TABLE trajet (id INT AUTO_INCREMENT NOT NULL, nom VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, adresse_depart VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        //$this->addSql('ALTER TABLE repere_trajet ADD CONSTRAINT FK_A59BDD6F2ECB75C FOREIGN KEY (repere_id) REFERENCES repere (id) ON UPDATE NO ACTION ON DELETE CASCADE');
        //$this->addSql('ALTER TABLE repere_trajet ADD CONSTRAINT FK_A59BDD6FD12A823 FOREIGN KEY (trajet_id) REFERENCES trajet (id) ON UPDATE NO ACTION ON DELETE CASCADE');
        //$this->addSql('ALTER TABLE course ADD trajet_id INT DEFAULT NULL');
        //$this->addSql('ALTER TABLE course ADD CONSTRAINT FK_169E6FB9D12A823 FOREIGN KEY (trajet_id) REFERENCES trajet (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        //$this->addSql('CREATE INDEX IDX_169E6FB9D12A823 ON course (trajet_id)');
    }
}
