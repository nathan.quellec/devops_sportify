<p align="center"><img src="https://upload.wikimedia.org/wikipedia/commons/a/a3/Logo_Universit%C3%A9_Paris-Nanterre.svg" width="400"></p>

# DEVOPS1 - Sportify

- Léa HABERT
- Yann LEMENN
- Nathan QUELLEC
- Hariprasad RAYPOULET
- Ophélie SCHAUFFLER

Ce projet est une application Andoird, nous allons voir les étapes à suivre pour installer le projet.
Nous utilisons Symfony et API Platform pour le Web Service.

## Prérequis
- PHP (version 7.3 minimum)
- Composer
- Symfony (vous pouvez éxécutr la commande ``symfony check:requirements`` pour vos assurer d'avoir les configurations nécessaires).
- MariaDB
- Un serveur web local (WAMP pour notre cas)

## Installation du web service

1. Récupérer la partie Web Service du projet soit en le téléchargeant directement depuis Gitlab soit en tapant la commande suivante dans votre répertoire :  
``git clone https://gitlab.com/lea_habert/sportifysymfony.git``  

2. Une fois le projet téléchargé, placez-vous à la racine de ce dernier puis tapez la commande :  
``composer install``  
Cela va installer toutes les dépendances du projet (peut prendre du temps en fonction de votre connexion internet)

3. Puis à la racine du projet, accédez au ficher .env pour ajouter les paramètres pour la connexion à la base de données :
- DB_HOST
- DB_PORT
- DB_DATABSE
- DB_USERNAME
- DB_PASSWORD

4. Vous devez ensuite créer une base de données vide (dont les paramètres sont ceux que vous avez indiqué précédemment dans le fichier .env).

5. Dans l'invit de commande, en étant placé à la racine du projet, éxécutez la commande suivante : 
``php bin/console make:migration`` . Ceci vous permettra de créer les tables et associations.

6. Toujours au même endroit, éxécutez la commande ``symfony server:start`` pour avoir accès aux requêtes de l'API.
