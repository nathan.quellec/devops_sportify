<p align="center"><img src="https://upload.wikimedia.org/wikipedia/commons/a/a3/Logo_Universit%C3%A9_Paris-Nanterre.svg" width="400"></p>

# DEVOPS - Sportify

- Léa HABERT
- Yann LEMENN
- Nathan QUELLEC
- Hariprasad RAYPOULET
- Ophélie SCHAUFFLER

##### Cette documentation permet de répertorier les commandes de base à connaître afin de faire de l'intégration continue, via GitLab dans notre cas.

*Toutes les commandes ci-dessous sont à éxécuter en se placant à la racine du projet.*

## 1 - Commencer à travailler sur un repository existant 

Pour commencer à travailler localement sur un repository git existant, il faut le clone avec la commande suivante : 
``git clone https://gitlab.com/nathan.quellec/devops_sportify.git``.

## 2 - Télécharger les derniers changements du projet

*Attention, cette étape est à effectuer **systématiquement** avant de travailler localement sur le projet.* 

Pour travailler sur une copie à jour du projet (il est important de le faire chaque fois que vous commencez à travailler sur un projet), vous *pull* pour obtenir toutes les modifications apportées par les utilisateurs depuis la dernière fois que vous avez *clone* ou *pull* le projet.

Voici la commande à éxécuter : ``git pull origin master``.


## 3 - Uploader les derniers changements locaux du projet

Après avoir *pull* puis avoir fait les changements locaux, voici les différentes commandes à éxécuter afin de partager les changements effectués localement sur GitLab.

- ``git add .``
- ``git commit -m "COMMENTAIRE POUR DÉCRIRE L'INTENTION DU COMMIT"``
- ``git push origin master``

## Informatiosns sur l'état du Git

``git status``.  
Cette commande est très utile pour être conscient de ce qui se passe et de l'état de vos modifications. Lorsque vous ajoutez, modifiez ou supprimez des fichiers / dossiers, Git le sait.  
Elle permet également de savoir sur quelle branch vous êtes.

## Changer de branch 

Dans les parties précédentes, toutes les commandes sont aplliquées sur la branch *master*.  
Il est possible de changer de branch et d'éxécuter les mêmes commandes en changeant *master* par le nom de votre branche, *myBranch* par exemple.

Créer une branch et aller dessus : ``git checkout -b myBranch``.  
Changer de branch (vers une branch existante) : ``git checkout myBranch``.
