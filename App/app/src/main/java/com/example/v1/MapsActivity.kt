package com.example.v1

import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.gms.maps.model.PolylineOptions
import com.google.android.material.tabs.TabLayout
import com.google.maps.android.PolyUtil
import org.json.JSONObject
import java.io.IOException

class MapsActivity : AppCompatActivity(), OnMapReadyCallback, GoogleMap.OnMarkerClickListener {


    private var googleMap: GoogleMap? = null
    private lateinit var map: GoogleMap
    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private lateinit var lastLocation: Location
    private var pointsList: MutableList<LatLng> = ArrayList()

    companion object {
        private const val LOCATION_PERMISSION_REQUEST_CODE = 1
    }



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_maps)
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val tabLayout: TabLayout = findViewById(R.id.tab)
        tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab?) {
                when (tab?.position) {
                    0 -> {
                        val intent = Intent(this@MapsActivity, MainActivity::class.java)
                        startActivity(intent)
                    }
                    else -> ""
                }
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {
            }

            override fun onTabReselected(tab: TabLayout.Tab?) {
            }
        })
        val tabLayout2: TabLayout = findViewById(R.id.tab2)
        tabLayout2.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab?) {
                when (tab?.position) {
                    0 -> {
                        map.clear();
                        pointsList.removeAll(pointsList)
                    }
                    1 -> {
                        if(pointsList.size == 2){
                            val path: MutableList<List<LatLng>> = ArrayList()
                            val urlDirections = "https://maps.googleapis.com/maps/api/directions/json?&mode=walking&origin=${pointsList.get(0).latitude},${pointsList.get(0).longitude}&destination=${pointsList.get(1).latitude},${pointsList.get(1).longitude}&key=AIzaSyA_XzKVgSEGTyDmqMXcy11DHKKIod3Y4-o"
                            println(urlDirections)

                            val directionsRequest = object : StringRequest(
                                Request.Method.GET,
                                urlDirections,
                                Response.Listener<String> { response ->
                                    val jsonResponse = JSONObject(response)
                                    // Get routes
                                    val routes = jsonResponse.getJSONArray("routes")
                                    val legs = routes.getJSONObject(0).getJSONArray("legs")
                                    val steps = legs.getJSONObject(0).getJSONArray("steps")
                                    for (i in 0 until steps.length()) {
                                        val points =
                                            steps.getJSONObject(i).getJSONObject("polyline").getString("points")
                                        path.add(PolyUtil.decode(points))
                                    }
                                    for (i in 0 until path.size) {
                                        map.addPolyline(PolylineOptions().addAll(path[i]).color(Color.RED))
                                    }
                                },
                                Response.ErrorListener { _ ->
                                }){}
                            val requestQueue = Volley.newRequestQueue(this@MapsActivity)
                            requestQueue.add(directionsRequest)

                        }
                    }
                    else -> ""
                }
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {
            }

            override fun onTabReselected(tab: TabLayout.Tab?) {
                when (tab?.position) {
                    0 -> {
                        map.clear();
                        pointsList.removeAll(pointsList)
                    }
                    1 -> {
                        if(pointsList.size == 2){
                            val path: MutableList<List<LatLng>> = ArrayList()
                            val urlDirections = "https://maps.googleapis.com/maps/api/directions/json?&mode=walking&origin=${pointsList.get(0).latitude},${pointsList.get(0).longitude}&destination=${pointsList.get(1).latitude},${pointsList.get(1).longitude}&key=AIzaSyA_XzKVgSEGTyDmqMXcy11DHKKIod3Y4-o"
                            println(urlDirections)

                            val directionsRequest = object : StringRequest(
                                Request.Method.GET,
                                urlDirections,
                                Response.Listener<String> { response ->
                                    val jsonResponse = JSONObject(response)
                                    // Get routes
                                    val routes = jsonResponse.getJSONArray("routes")
                                    val legs = routes.getJSONObject(0).getJSONArray("legs")
                                    val steps = legs.getJSONObject(0).getJSONArray("steps")
                                    for (i in 0 until steps.length()) {
                                        val points =
                                            steps.getJSONObject(i).getJSONObject("polyline").getString("points")
                                        path.add(PolyUtil.decode(points))
                                    }
                                    for (i in 0 until path.size) {
                                        map.addPolyline(PolylineOptions().addAll(path[i]).color(Color.RED))
                                    }
                                },
                                Response.ErrorListener { _ ->
                                }){}
                            val requestQueue = Volley.newRequestQueue(this@MapsActivity)
                            requestQueue.add(directionsRequest)

                        }
                    }
                    else -> ""
                }
            }
        })
        val mapFragment = supportFragmentManager
                .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    /*override fun onMapReady(googleMap: GoogleMap) {
        map = googleMap

        // Add a marker in Sydney and move the camera
        val nanterre = LatLng(48.903639, 2.213378)
        map.addMarker(MarkerOptions().position(nanterre).title("Université de Paris Nanterre"))
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(nanterre, 12.0f))
        map.getUiSettings().setZoomControlsEnabled(true)
        map.setOnMarkerClickListener(this)
        setUpMap()
    }*/





    override fun onMapReady(googleMap: GoogleMap) {
        map = googleMap
        map.uiSettings.isZoomControlsEnabled = true
        map.setOnMarkerClickListener(this)
        setUpMap()
        val latLngOrigin = LatLng(48.89257049560547, 2.235877513885498) // La défense
        val latLngDestination = LatLng(48.902704351740056, 2.2134177542213753) // Université Paris Nanterre
        map.addMarker(MarkerOptions().position(latLngOrigin).title("La défense"))
        map.addMarker(
            MarkerOptions().position(latLngDestination).title("Université de Paris Nanterre")
        )



        //map!!.moveCamera(CameraUpdateFactory.newLatLngZoom(latLngOrigin, 14.5f))
        val path: MutableList<List<LatLng>> = ArrayList()
        val urlDirections = "https://maps.googleapis.com/maps/api/directions/json?&mode=walking&origin=48.89257049560547,2.235877513885498&destination=48.902704351740056,2.2134177542213753&key=AIzaSyA_XzKVgSEGTyDmqMXcy11DHKKIod3Y4-o"
        val directionsRequest = object : StringRequest(
            Request.Method.GET,
            urlDirections,
            Response.Listener<String> { response ->
                val jsonResponse = JSONObject(response)
                // Get routes
                val routes = jsonResponse.getJSONArray("routes")
                val legs = routes.getJSONObject(0).getJSONArray("legs")
                val steps = legs.getJSONObject(0).getJSONArray("steps")
                for (i in 0 until steps.length()) {
                    val points =
                        steps.getJSONObject(i).getJSONObject("polyline").getString("points")
                    path.add(PolyUtil.decode(points))
                }
                for (i in 0 until path.size) {
                    map.addPolyline(PolylineOptions().addAll(path[i]).color(Color.RED))
                }
            },
            Response.ErrorListener { _ ->
            }){}
        val requestQueue = Volley.newRequestQueue(this)
        requestQueue.add(directionsRequest)

        map.setOnMapClickListener(object : GoogleMap.OnMapClickListener {
            override fun onMapClick(latlng: LatLng) {
                // Clears the previously touched position
                //map.clear();
                // Animating to the touched position
                map.animateCamera(CameraUpdateFactory.newLatLng(latlng));

                val location = LatLng(latlng.latitude, latlng.longitude)
                if(pointsList.size < 2){
                    pointsList.add(location)
                    println(pointsList.size)
                    map.addMarker(MarkerOptions().position(location))
                }

            }
        })

    }

    override fun onMarkerClick(p0: Marker?): Boolean {
        p0?.remove()
        pointsList.remove(pointsList.last())
        return true;
    }

    private fun placeMarkerOnMap(location: LatLng) {
        val markerOptions = MarkerOptions().position(location)

        val titleStr = getAddress(location)  // add these two lines
        markerOptions.title(titleStr)

        map.addMarker(markerOptions)
    }

    private fun setUpMap() {
        if (ActivityCompat.checkSelfPermission(
                this,
                android.Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION),
                LOCATION_PERMISSION_REQUEST_CODE
            )
            return
        }
        map.isMyLocationEnabled = true

// 2
        fusedLocationClient.lastLocation.addOnSuccessListener(this) { location ->
            // Got last known location. In some rare situations this can be null.
            // 3
            if (location != null) {
                lastLocation = location
                val currentLatLng = LatLng(location.latitude, location.longitude)
                map.animateCamera(CameraUpdateFactory.newLatLngZoom(currentLatLng, 12f))
            }
        }
    }

    private fun getAddress(latLng: LatLng): String {
        // 1
        val geocoder = Geocoder(this)
        val addresses: List<Address>?
        val address: Address?
        var addressText = ""

        try {
            // 2
            addresses = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 1)
            // 3
            if (null != addresses && !addresses.isEmpty()) {
                address = addresses[0]
                for (i in 0 until address.maxAddressLineIndex) {
                    addressText += if (i == 0) address.getAddressLine(i) else "\n" + address.getAddressLine(
                        i
                    )
                }
            }
        } catch (e: IOException) {
            Log.e("MapsActivity", e.localizedMessage)
        }

        return addressText
    }
}